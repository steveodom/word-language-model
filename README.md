# Instance Type for GPU:
I used: p2.xlarge. super fast.

# login
ssh -v -i pytorch.pem ubuntu@54.237.238.183

# copy files up
scp -i ../../pytorch.pem -r images.zip ubuntu@34.228.20.229:~/imgnet-01/data/

#Example Train Command:
python3 main_pf.py --data=./data/short/ --epochs=1

#Example Generate Command:
python3 generate_pf.py --data=./data/short/ --checkpoint=models/2018-01-24T12-54-08/model-LSTM-emsize-200-nhid_200-nlayers_2-batch_size_20-epoch_1-loss_6.59-ppl_729.43.pt

# Word-level language modeling RNN

This example trains a multi-layer RNN (Elman, GRU, or LSTM) on a language modeling task.
By default, the training script uses the PTB dataset, provided.
The trained model can then be used by the generate script to generate new text.

```bash
python main.py --cuda  # Train an LSTM on ptb with cuda (cuDNN). Should reach perplexity of 113
python generate.py     # Generate samples from the trained LSTM model.
```

The model uses the `nn.RNN` module (and its sister modules `nn.GRU` and `nn.LSTM`)
which will automatically use the cuDNN backend if run on CUDA with cuDNN installed.

The `main.py` script accepts the following arguments:

```bash
optional arguments:
  -h, --help         show this help message and exit
  --data DATA        location of the data corpus
  --model MODEL      type of recurrent net (RNN_TANH, RNN_RELU, LSTM, GRU)
  --emsize EMSIZE    size of word embeddings
  --nhid NHID        humber of hidden units per layer
  --nlayers NLAYERS  number of layers
  --lr LR            initial learning rate
  --clip CLIP        gradient clipping
  --epochs EPOCHS    upper epoch limit
  --batch-size N     batch size
  --bptt BPTT        sequence length
  --seed SEED        random seed
  --cuda             use CUDA
  --log-interval N   report interval
  --save SAVE        path to save the final model
```
